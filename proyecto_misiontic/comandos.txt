//comando para crear proyectos en react
npx create-react-app nombre-proyecto

//comando para inicializar el servidor
npm start

//comando para detener servidor
presionar tecla control + la tecla de la letra c ctrl + c

//instalar tema ADMINLTE 3
npm install admin-lte@^3.2 --save

//instalar react routing
npm install --save react-router-dom

//instalar paquete de alertas sweetalert
npm install sweetalert

//comando para generar la version de produccion del proyecto
npm run build

//comando para instalar node_modules
npm install

