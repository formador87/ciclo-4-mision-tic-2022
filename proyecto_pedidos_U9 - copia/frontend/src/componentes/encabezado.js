import {Link} from 'react-router-dom';
function Encabezado()
{
    return(
        <header className="main-header-top hidden-print">
        <a href="index.html" className="logo"><img className="img-fluid able-logo" src="assets/images/logo.png" alt="Theme-logo"></img></a>
        <nav className="navbar navbar-static-top">
            <a href="#!" data-toggle="offcanvas" className="sidebar-toggle"></a>
            <ul className="top-nav lft-nav">
            </ul>
        </nav>
        </header>
    )
}
export default Encabezado;

