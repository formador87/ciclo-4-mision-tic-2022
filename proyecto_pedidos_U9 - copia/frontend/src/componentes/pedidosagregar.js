import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function PedidosAgregar()
{
    const[id_cliente, setIdCliente] = useState('')
    const[fecha, setFecha] = useState('')
    const[valor, setValor] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function pedidosInsertar()
    {

        const pedidoinsertar = {
            id: uniquid(),
            id_cliente: id_cliente,
            fecha: fecha,
            valor: valor,
            activo: activo
        }

        console.log(pedidoinsertar)

        axios.post(`/api/pedidos/agregar`,pedidoinsertar,).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function pedidosRegresar()
    {
        //window.location.href="/";
        navigate('/pedidoslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Pedido</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id_cliente" className="form-label">Id Cliente</label>
                        <input type="text" className="form-control" id="id_cliente" value={id_cliente} onChange={(e) => {setIdCliente(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="valor" className="form-label">Valor</label>
                        <input type="text" className="form-control" id="valor" value={valor} onChange={(e) => {setValor(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={pedidosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={pedidosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default PedidosAgregar;