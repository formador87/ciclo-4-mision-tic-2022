import React, {Fragment} from 'react';
import { BrowserRouter, Routes, Route} from 'react-router-dom';

import Menu from './componentes/menu';
import PaginaPrincipal from './componentes/paginaprincipal';
import PedidosListar from './componentes/pedidoslistar';
import PedidosEditar from './componentes/pedidoseditar';
import PedidosBorrar from './componentes/pedidosborrar';
import PedidosAgregar from './componentes/pedidosagregar';
import ClientesCombo from './componentes/clientescombo';

import Login from './componentes/login';
import Register from './componentes/register';
function App()
{
  return (
    <main className='App'>
      <Fragment>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Login/>} exact/>
            <Route path="/register" element={<Register/>} exact/>
            <Route path="/paginaprincipal" element={<PaginaPrincipal/>} exact/>
            <Route path="/pedidoslistar" element={<PedidosListar/>} exact/>
            <Route path="/pedidoseditar" element={<PedidosEditar/>} exact/>
            <Route path="/pedidosborrar" element={<PedidosBorrar/>} exact/>
            <Route path="/pedidosagregar" element={<PedidosAgregar/>} exact/>
            <Route path="/clientescombo" element={<ClientesCombo/>} exact/>
          </Routes>
        </BrowserRouter>
      </Fragment>
    </main>
  );
}
export default App;
