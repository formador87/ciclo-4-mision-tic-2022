const express = require('express');
const router = express.Router();
const modeloPedido = require('../models/model_pedidos');

router.get('/listar', (req, res) => {
    modeloPedido.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.get('/cargar/:id', (req, res) => {
    modeloPedido.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar',(req,res)=>{
    const nuevoPedido = new modeloPedido({
        id : req.body.id,
        fecha : req.body.fecha,
        valor : req.body.valor,
        activo : req.body.activo
    });
    nuevoPedido.save(function(err)
    {
        if(!err)
        {
            res.send("El registro se agregó exitosamente");
        }
        else
        {
            res.send(err);
        }
    }
    );
});

router.post('/editar/:id',(req,res)=>{
modeloPedido.findOneAndUpdate({id:req.params.id},
    {
        fecha : req.body.fecha,
        valor : req.body.valor,
        activo : req.body.activo
    },(err) =>
    {
        if(!err)
        {
            res.send("El registro se editó exitosamente");
        }
        else
        {
            res.send(err);
        }
    })
});

router.delete('/borrar/:id',(req,res)=>{
    modeloPedido.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El registro se borró exitosamente");
            }
            else
            {
                res.send(err);
            }
        })
    });

module.exports = router;