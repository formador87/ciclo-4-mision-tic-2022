const express = require('express');
const router = express.Router();

const controladorPedidosClientes = require('../controllers/controller_pedidosclientes');
router.get("/pedidosclientes",controladorPedidosClientes);
router.get("/pedidosclientes/:id",controladorPedidosClientes);



module.exports = router;

