const express = require('express')
const router = express.Router()
const modeloProducto = require('../models/model_productos')

//Peticion get para prueba
router.get('/prueba', (req, res) => {
    res.end("Prueba de ruta OK!")
})

//Cargar un producto http://localhost:5000/api/productos/cargar/
router.get('/listar', (req, res) => {
    modeloProducto.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/agregar/
router.post('/agregar', (req, res) => {
    
    const nuevoProducto = new modeloProducto({
        id: req.body.id,
        id_categoria: req.body.id_categoria,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        precio: req.body.precio,
        activo: req.body.activo
    })
    //nuevoProducto._id = new mongoose.Types.ObjectId();
    //nuevoProducto.set('versionKey', false);
    nuevoProducto.save(function(err)
    {
        if(!err)
        {
            res.send('El producto fue agregado exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un producto http://localhost:5000/api/productos/cargar/1
router.get('/cargar/:id', (req, res) => {
    modeloProducto.find({id:req.params.id}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/editar/2
router.post('/editar/:id', (req, res) => {
    modeloProducto.findOneAndUpdate(
        {id:req.params.id}
        ,{
            id_categoria:req.body.id_categoria,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            precio: req.body.precio,
            activo: req.body.activo
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El producto se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/productos/borrar/3
router.delete('/borrar/:id', (req, res) => {
    modeloProducto.findOneAndDelete(
        {id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El producto se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router