const modeloProductos = require('../models/model_productos');

exports.productosAgregar = async (req, res) => {
  const nuevoProducto = new modeloProductos({
      id: req.body.id,
      id_categoria: req.body.id_categoria,
      nombre: req.body.nombre,
      descripcion: req.body.descripcion,
      valor: req.body.valor,
      estado: req.body.estado
  })
  nuevoProducto.save()
    res.json({nuevoProducto});
}

exports.productosListar = async (req, res) => {
  try {
    const productos = await modeloProductos.find();
    res.json({productos})
  } catch (error) {
        res.status(400).send("Hubo un error");
  }
}

exports.productosCargar = async (req, res) => {
  try {
    const productos = await modeloProductos.find({id:req.params.id});
  res.json({productos})
  } catch (error) {
    res.status(400).send("Hubo un error");
  }
}

exports.productosEditar = async (req, res) => {
await modeloProductos.findOneAndUpdate(
      {id:req.params.id}
      ,{
        id: req.body.id,
        id_categoria: req.body.id_categoria,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        valor: req.body.valor,
        estado: req.body.estado
      })
      res.json({msg: "actualizar el registro"})
}

/*
const { id, id_categoria, nombre,descripcion, valor } = req.body;

    const nuevoProducto = {};
    if (id){
        nuevoProducto.id = id, 
        nuevoProducto.id_categoria = id_categoria,
        nuevoProducto.nombre = nombre,
        nuevoProducto.descripcion = descripcion,
        nuevoProducto.valor = valor
    }
*/

exports.productosBorrar = async (req, res) => {
    await modeloProductos.findOneAndDelete(
      {id:req.params.id})
      res.json({msg: "producto eliminado "})
}