"use strict";

var express = require('express');

var router = express.Router();

var controladorProductos = require('./router_productos');

router.use("/productos", controladorProductos);

var controladorProductosSinAuth = require('./router_productos');

router.use("/productossinauth", controladorProductosSinAuth);

var rutaUsuarios = require('./router_usuarios');

router.use("/usuarios", rutaUsuarios);

var rutaAuth = require('./router_auth');

router.use("/auth", rutaAuth);
module.exports = router;