const modeloCategoria = require('../models/model_categorias');
const modeloProducto = require('../models/model_productos');
const miconexion = require('../conexion');

//CONSULTA USANDO EL METODO AGGREGATE DE MONGODB
modeloCategoria.aggregate([{
    $lookup: {
        localField: "id",
        from: "productos",
        foreignField: "id_categoria",
        as: "productos_categoria",
    },
},
{
    $unwind: "$productos_categoria",
}
])
.then((result)=>{console.log(result)})
.catch((error)=>{console.log(error)});

//CONSULTA CON METODOS JAVASCRIPT Y MONGODB FILTRANDO POR ALGUN CAMPO DE LA COLECCION
var dataCategorias = [];
modeloCategoria.find({ nombre: "Comestibles" }).then(data => {
        console.log("Categoria:")
        console.log(data);
        data.map((d, k) => {dataCategorias.push(d.id);})
        modeloProducto.find({ id_categoria: { $in: dataCategorias } })
            .then(data => {
                console.log("Productos de la categoria:")
                console.log(data);
            })
            .catch(error => {
                console.log(error);
            })
    })
    .catch(error => {
        console.log(error);
    })

/*
https://www.mongodb.com/docs/manual/core/aggregation-pipeline/
db.orders.aggregate( [
   // Stage 1: Filter pizza order documents by date range
   {
      $match:
      {
         "date": { $gte: new ISODate( "2020-01-30" ), $lt: new ISODate( "2022-01-30" ) }
      }
   },
   // Stage 2: Group remaining documents by date and calculate results
   {
      $group:
      {
         _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
         totalOrderValue: { $sum: { $multiply: [ "$price", "$quantity" ] } },
         averageOrderQuantity: { $avg: "$quantity" }
      }
   },
   // Stage 3: Sort documents by totalOrderValue in descending order
   {
      $sort: { totalOrderValue: -1 }
   }
 ] )
 */


 /*
const modeloClientes = require('../models/model_clientes');
const modeloPedidos = require('../models/model_pedidos');

const miconexion = require('../conexion');
*/
/*
//RELACION ENTRE COLECCIONES UTILIZANDO EL METODO AGGREGATE DE MONGODB
modeloClientes.aggregate([
    {
        $lookup: {
            localField: "id",
            from: "pedidos",
            foreignField: "id_cliente",
            as: "pedidos_clientes",
        },
    },
    { $unwind: "$pedidos_clientes" }])
    .then((result)=>{console.log(result)})
    .catch((error)=>{console.log(error)});
*/
/*
//RELACION ENTRE COLECCIONES UTILIZANDO METODOS BASICOS DE MONGODB Y ARREGLOS
var dataClientes = [];
modeloClientes.find({id: "1098241555"}).then(data => {
    console.log("Datos del Cliente:");
    console.log(data);
    data.map((d, k) => {dataClientes.push(d.id);})
    modeloPedidos.find({id_cliente: { $in: dataClientes}})
    .then(data => {
        console.log("Pedidos del cliente:");
        console.log(data);
    })
    .catch((error)=>{console.log(error)});
});
*/