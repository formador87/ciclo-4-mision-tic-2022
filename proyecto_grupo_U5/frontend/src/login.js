import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import APIInvoke from '../utils/APIInvoke';
import swal from 'sweetalert2';

const Login = () => {

    //para redireccionar de un componente a otro
    const navigate = useNavigate();

    //definimos el estado inicial de las variables
    const [usuario, setUsuario] = useState({
        email: '',
        password: ''
    });

    const { email, password } = usuario;

    const onChange = (e) => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        document.getElementById("email").focus();
    }, [])

    const iniciarSesion = async () => {
        if (password.length < 6) {
            const msg = "La contraseña debe ser al menos de 6 caracteres.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            const data = {
                email: usuario.email,
                password: usuario.password
            }
            const response = await APIInvoke.invokePOST(`/api/auth`, data);
            const mensaje = response.msg;

            if (mensaje === 'El usuario no existe' || mensaje === 'Contraseña incorrecta') {
                const msg = "No fue posible iniciar la sesión verifique los datos ingresados.";
                swal({
                    title: 'Error',
                    text: msg,
                    icon: 'error',
                    buttons: {
                        confirm: {
                            text: 'Ok',
                            value: true,
                            visible: true,
                            className: 'btn btn-danger',
                            closeModal: true
                        }
                    }
                });
            } else {
                //obtenemos el token de acceso jwt
                const jwt = response.token;

                //guardamos el token en el localstorage
                localStorage.setItem('token', jwt);

                //redireccionamos al home la pagina principal
                navigate("/home");
            }
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        iniciarSesion();
    }

    return(
    <div className="auth-wrapper">
        <div className="auth-content text-center">
            <img src="assets/images/logo.png" alt="" className="img-fluid mb-4"></img>
            <div className="card borderless">
                <div className="row align-items-center ">
                    <div className="col-md-12">
                        <div className="card-body">
                            <h4 className="mb-3 f-w-400">Iniciar sesión</h4>
                            <hr></hr>
                            <form onSubmit={onSubmit}>
                            <div className="form-group mb-3">
                                <input type="text" 
                                    className="form-control"
                                    placeholder="Email"
                                    id="email"
                                    name="email"
                                    value={email}
                                    onChange={onChange}
                                    required />
                            </div>
                            <div className="form-group mb-4">
                                <input type="password" 
                                className="form-control"
                                placeholder="Contraseña"
                                id="password"
                                name="password"
                                value={password}
                                onChange={onChange}
                                required />
                            </div>
                            <div className="custom-control custom-checkbox text-left mb-4 mt-2">
                                <input type="checkbox" className="custom-control-input" id="customCheck1"></input>
                                <label className="custom-control-label" htmlFor="customCheck1">Recordarme</label>
                            </div>
                            <button type='submit' className="btn btn-block btn-primary mb-4">Signin</button>
                            <hr></hr>
                            <p className="mb-2 text-muted">Olvidaste el password?<Link to={"/reset"} className="f-w-400">Restablecer</Link></p>
                            <p className="mb-0 text-muted">No tienes una cuenta<Link to={"/register"} className="f-w-400">Registrate</Link></p>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default Login;
