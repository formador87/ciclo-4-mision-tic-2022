
import {Link} from 'react-router-dom';

function Logo()
{

    return(
        <div>
            <header className="navbar pcoded-header navbar-expand-lg navbar-light header-dark">
            <div className="m-header">
                <a className="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
                <a href="#!" className="b-brand">
                    <img src="assets/images/logo.png" alt="" className="logo"></img>
                    <img src="assets/images/logo-icon.png" alt="" className="logo-thumb"></img>
                </a>
                <a href="#!" className="mob-toggler">
                    <i className="feather icon-more-vertical"></i>
                </a>
            </div>
            </header>
        </div>
    )
}

export default Logo


