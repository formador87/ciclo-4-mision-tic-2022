import React from 'react';
import Menu from '../plantilla/menu';
import Logo from '../plantilla/logo';
import {Link} from 'react-router-dom';

function PaginaPrincipal()
{
    return(
        <div className="">
            <Menu/>
            <Logo/>
            <section className="pcoded-main-container">
            <div className="pcoded-content">
                <div className="page-header">
                    <div className="page-block">
                        <div className="row align-items-center">
                            <div className="col-md-12">
                                <div className="page-header-title">
                                    <h5 className="m-b-10">Mi tienda virtual</h5>
                                </div>
                                <ul className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to={"/inicio"}><i className="feather icon-home"></i></Link></li>
                                    <li className="breadcrumb-item"><Link to={"#"}></Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section> 
        </div>   
    )
}
export default PaginaPrincipal;