import {Link} from 'react-router-dom';

function Register()
{

    return(

    <div className="auth-wrapper">
        <div className="auth-content text-center">
            <img src="assets/images/logo.png" alt="" className="img-fluid mb-4"></img>
            <div className="card borderless">
                <div className="row align-items-center text-center">
                    <div className="col-md-12">
                        <div className="card-body">
                            <h4 className="f-w-400">Sign up</h4>
                            <hr></hr>
                            <div className="form-group mb-3">
                                <input type="text" className="form-control" id="Username" placeholder="Username"></input>
                            </div>
                            <div className="form-group mb-3">
                                <input type="text" className="form-control" id="Email" placeholder="Email address"></input>
                            </div>
                            <div className="form-group mb-4">
                                <input type="password" className="form-control" id="Password" placeholder="Password"></input>
                            </div>
                            <div className="custom-control custom-checkbox  text-left mb-4 mt-2">
                                <input type="checkbox" className="custom-control-input" id="customCheck1"></input>
                                <label className="custom-control-label" for="customCheck1">Send me the <a href="#!"> Newsletter</a> weekly.</label>
                            </div>
                            <button className="btn btn-primary btn-block mb-4">Sign up</button>
                            <hr></hr>
                            <p className="mb-2">Already have an account? <a href="auth-signin.html" className="f-w-400">Signin</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )

}

export default Register
