import axios from 'axios';
import Swal from 'sweetalert2';
import React, {useState, useEffect} from 'react';
//import {useNavigate} from 'react-router'
function ProductosBorrar(id)
{    
    //const navegar = useNavigate()

    function productosRefrescar()
    {
        //navegar('/')
        window.location.href="/productoslistar";
    }

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
            buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({title: '¿Realmente desea eliminar este registro?',
        text: "No es posible revertir este cambio",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        reverseButtons: false
        }).then((result) => {
        if (result.isConfirmed) {
        
        
        const token = localStorage.getItem("token");
        let bearer;
        if (token === "") {
            bearer = "";
        } else {
            bearer = `${token}`;
        }
        const config = {
            headers: {'Content-Type': 'application/json', 'x-auth-token': bearer}
        }

        axios.delete(`/api/productos/borrar/${id}`,config)
        .then(() => this.setState({ status: 'Borrado Exitoso' }));
        
        /*
        swalWithBootstrapButtons.fire(
            '¡Operación Exitosa!',
            'El registro ha sido eliminado exitosamente',
            'success'
        )
        */
        
        productosRefrescar();

    } else if (
        result.dismiss === Swal.DismissReason.cancel
        ) {
        swalWithBootstrapButtons.fire(
            '¡ERROR!',
            'El registro no pudo ser eliminado',
            'error'
            )
        }
    })

    //useEffect(()=>{productosRefrescar()},[]);

}

export default ProductosBorrar;