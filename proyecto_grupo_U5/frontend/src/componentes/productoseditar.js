import axios from 'axios';
import React, {useEffect, useState} from 'react';
import { useParams, useNavigate } from 'react-router';
import Swal from 'sweetalert2';
import Menu from '../plantilla/menu';
import Logo from '../plantilla/logo';

function ProductosEditar()
{
    const parametros = useParams()

    const[id_categoria,setIdCategoria] = useState('')
    const[nombre,setNombre] = useState('')
    const[precio,setPrecio] = useState('')
    const[activo,setActivo] = useState('')
    const navegar = useNavigate()

    const token = localStorage.getItem("token");
    let bearer;
    if (token === "") {
        bearer = "";
    } else {
        bearer = `${token}`;
    }
    const config = {
        headers: {'Content-Type': 'application/json', 
        'x-auth-token': bearer}
    }

    useEffect(()=>{axios.get(`/api/productos/cargar/${parametros.id}`,config).then(res=>{
        console.log(res.data[0])
        const dataProductos = res.data[0]
        setIdCategoria(dataProductos.id_categoria)
        setNombre(dataProductos.nombre)
        setPrecio(dataProductos.precio)
        setActivo(dataProductos.activo)
    })},[])

    function productosActualizar()
    {
        const productoeditar = {
        id: parametros.id,
        id_categoria: id_categoria,
        nombre: nombre,
        precio: precio,
        activo: activo
        }

        console.log(productoeditar)

        const config = {
            body: JSON.stringify(productoeditar),
            headers: {'Content-Type': 'application/json', 'x-auth-token': bearer}
        }

        axios.post(`/api/productos/editar/${parametros.id}`,productoeditar,config).then(res=> {
            console.log(res.data)

            Swal.fire({ position: 'center', icon: 'success', title: '¡Registro actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navegar('/productoslistar')
        }).catch(err => {console.log(err.stack)})
        

    }

    function productosRegresar()
    {
        navegar('/productoslistar')
    }

    return(
    <div className="">
    <Menu/>
    <Logo/>
        <section className="pcoded-main-container">
            <div className="pcoded-content">
                <div className="col-sm-12">
                    <div className="card">
                        
                        
                        <div className="card-body">
                            <h5 className="mt-5">PRODUCTO</h5>
                            <form>
                                <div className="form-group">
                                    <label htmlFor="id_categoria">Categoria</label>
                                    <input type="text" className="form-control"id="id_categoria" value={id_categoria} onChange={(e)=>{setIdCategoria(e.target.value)}} placeholder="Categoria"></input>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="nombre">Nombre</label>
                                    <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}} placeholder="Nombre"></input>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="precio">Precio</label>
                                    <input type="text" className="form-control" id="precio" value={precio} onChange={(e)=>{setPrecio(e.target.value)}}  placeholder="Precio"></input>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="activo">Activo</label>
                                    <input type="text" className="form-control" id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}} placeholder="Activo"></input>
                                </div>
                                <button type="button" className="btn btn-info" onClick={productosRegresar}>Regresar</button>
                                <button type="button" className="btn btn-success" onClick={productosActualizar}>Actualizar</button>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>
    )

}

/*
<div className='container mt-5'>
        <h4>Producto</h4>
        <div className='row'>
            <div className='col-md-12'>
                <div className="mb-3">
                    <label htmlFor="id_categoria" className="form-label">Categoria</label>
                    <input type="text" className="form-control" id="id_categoria" value={id_categoria} onChange={(e)=>{setIdCategoria(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="nombre" className="form-label">Nombre</label>
                    <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="precio" className="form-label">Precio</label>
                    <input type="text" className="form-control" id="precio" value={precio} onChange={(e)=>{setPrecio(e.target.value)}}></input>
                </div>
                <div className="mb-3">
                    <label htmlFor="activo" className="form-label">Activo</label>
                    <input type="text" className="form-control" id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}}></input>
                </div>
                <button type="button" className="btn btn-info" onClick={productosRegresar}>Atras</button>
                <button type="button" className="btn btn-success" onClick={productosActualizar}>Actualizar</button>
            </div>
        </div>
    </div>
*/


/*
    <div className="mb-3 form-check">
        <input type="checkbox" className="activo" id="activo"></input>
        <label className="form-check-label" htmlFor="activo">Activo</label>
    </div>
*/


//FORMULARIO COMPLETO
/*
        <section className="pcoded-main-container">
            <div className="pcoded-content">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="mt-5">Form Grid</h5>
                            <form>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label htmlFor="inputEmail4">Email</label>
                                        <input type="email" className="form-control" id="inputEmail4" placeholder="Email"></input>
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label htmlFor="inputPassword4">Password</label>
                                        <input type="password" className="form-control" id="inputPassword4" placeholder="Password"></input>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress">Address</label>
                                    <input type="text" className="form-control" id="inputAddress" placeholder="1234 Main St"></input>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="inputAddress2">Address 2</label>
                                    <input type="text" className="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor"></input>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label htmlFor="inputCity">City</label>
                                        <input type="text" className="form-control" id="inputCity"></input>
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label htmlFor="inputState">State</label>
                                        <select id="inputState" className="form-control">
                                            <option selected>select</option>
                                            <option>Large select</option>
                                        </select>
                                    </div>
                                    <div className="form-group col-md-2">
                                        <label htmlFor="inputZip">Zip</label>
                                        <input type="text" className="form-control" id="inputZip"></input>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox" id="gridCheck"></input>
                                        <label className="form-check-label" htmlFor="gridCheck">Check me out</label>
                                    </div>
                                </div>
                                <button type="submit" className="btn  btn-primary">Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
*/

export default ProductosEditar;